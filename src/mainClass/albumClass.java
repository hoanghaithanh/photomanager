package mainClass;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.io.FileUtils;

public class albumClass {
public void createAlbum()
{
	String workingDir = System.getProperty("user.dir");
	String albumName = "";
	albumName = JOptionPane.showInputDialog("Nhap ten album");
	if(albumName.equals("")){
		JOptionPane.showMessageDialog(null, "Ten Album khong duoc de trong");
		return;
	}
	File albumfolder = new File(workingDir+"\\album\\"+albumName);
	if(albumfolder.exists())
	{
		JOptionPane.showMessageDialog(null,"Da ton tai Album nay");
}else
{
	albumfolder.mkdirs();
	if(!albumfolder.exists())
	{
		JOptionPane.showMessageDialog(null, "Ten da nhap khong hop le");
	}
}
}

public void addFileToAlbum(){
	JOptionPane.showMessageDialog(null, "Chon Album");
	JFileChooser chooser4 = new JFileChooser();
	chooser4.setCurrentDirectory(new File(System.getProperty("user.dir")+"\\album"));
	
	chooser4.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	int returnValue4 = chooser4.showOpenDialog(null);
      if(returnValue4 == JFileChooser.APPROVE_OPTION) {  
   String PathFolder = chooser4.getSelectedFile().getPath();
   File file4 = new File(PathFolder);
   if (!file4.getParent().equals(System.getProperty("user.dir")+"\\album"))
   {
	   JOptionPane.showMessageDialog(null, "Album khong hop le");
	   return;
   }
   JOptionPane.showMessageDialog(null, "Chon anh them vao album");
   JFileChooser chooser = new JFileChooser();
   chooser.setCurrentDirectory(new File("C:\\Users\\hoang\\Pictures"));
	FileFilter filter = new FileNameExtensionFilter("Image files", "jpg", "gif","bmp","png");
	chooser.setFileFilter(filter);
	chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
	int returnVal = chooser.showOpenDialog(null);
      if(returnVal == JFileChooser.APPROVE_OPTION) {  
    	  File file5 = chooser.getSelectedFile();
    	  File file6 = new File(file4.getPath()+"\\"+file5.getName());
    	  if(file6.exists())
    	  {
    		  JOptionPane.showMessageDialog(null, "Da ton tai file co ten "+file5.getName()+" trong Album " + file4.getName());
    		  return;
    	  }else{
    	  try {
			
			FileUtils.copyFileToDirectory(file5, file4);
			if(file6.exists()){
				JOptionPane.showMessageDialog(null, "Them anh thanh cong!");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
      }
      }
      }
}
public void albumFilter(){
	JFileChooser chooser7 = new JFileChooser();
	chooser7.setCurrentDirectory(new File(System.getProperty("user.dir")+"\\album"));
	chooser7.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	int returnVal7 = chooser7.showOpenDialog(null);
      if(returnVal7 == JFileChooser.APPROVE_OPTION) {  
   File choosedAlbum = chooser7.getSelectedFile();
   if(!choosedAlbum.getParent().equals(System.getProperty("user.dir")+"\\album")) return;
	String stringfrom = JOptionPane.showInputDialog("Tu ngay (dd-mm-yyyy): ");
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	Date dayfrom =null;
	if(!stringfrom.equals("")){
	try {
	    //Parsing the String
		dateFormat.setLenient(false);
	    dayfrom = dateFormat.parse(stringfrom);
	   
	} catch (ParseException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	    JOptionPane.showMessageDialog(null, "Ngay bat dau khong hop le");
	    return;
	}
	}
	String stringto = JOptionPane.showInputDialog("Den ngay (dd-mm-yyyy): ");
	Date dayto = null;
	if(!stringto.equals("")){
	try {
	    //Parsing the String
		dateFormat.setLenient(false);
	    dayto = dateFormat.parse(stringto);
	} catch (ParseException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	    JOptionPane.showMessageDialog(null, "Ngay den khong hop le");
	    return;
	}
	}
	DateFileFilter dateFileFilter = new DateFileFilter(dayfrom, dayto);
	chooser7.setFileFilter(dateFileFilter);
	chooser7.setCurrentDirectory(choosedAlbum);
	chooser7.setFileSelectionMode(JFileChooser.FILES_ONLY);
	chooser7.showDialog(null, "Done");
      }
}

      
public boolean deleteAlbum()
{ 
	JFileChooser chooser3 = new JFileChooser();
	chooser3.setCurrentDirectory(new File(System.getProperty("user.dir")+"\\album"));
	
	chooser3.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	int returnValue3 = chooser3.showOpenDialog(null);
      if(returnValue3 == JFileChooser.APPROVE_OPTION) {
    	  File file3= chooser3.getSelectedFile();
    	  System.out.println(file3.getParentFile().getPath());
    	  System.out.println(System.getProperty("user.dir")+"\\album");
    	  if(!file3.getParentFile().getPath().equals(System.getProperty("user.dir")+"\\album"))
    	  {
    		  JOptionPane.showMessageDialog(null, "Album da chon khong hop le");
    	  }
    	  else
    	  {
    		  int reply = JOptionPane.showConfirmDialog(null, "Ban co that su muon xoa album " + file3.getName()+ " khong?");
    		  if(reply == JOptionPane.YES_OPTION)
    		  {
    			  String[]entries = file3.list();
    			  for(String s: entries){
    			      File currentFile = new File(file3.getPath(),s);
    			      currentFile.delete();
    			  }
    			  file3.delete();
    			  return !file3.exists();
}
    	  }
      }
	return false;
}
}
