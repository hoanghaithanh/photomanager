package mainClass;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.filechooser.FileFilter;

public class DateFileFilter extends FileFilter  
{    private Date dateFrom = null;
	 private Date dateTo = null;
	 public DateFileFilter(Date from, Date to)
	 {
		 dateFrom = from;
		 dateTo = to;
	 }
     public boolean accept(File file)  
     {   

          String stringpath = file.getPath();
          java.nio.file.Path path = Paths.get(stringpath);
  		BasicFileAttributes view = null;
  		try {
  		    view = Files.getFileAttributeView( path, BasicFileAttributeView.class ).readAttributes();
  		} catch ( IOException ex ) {
  		    
  		}
          //Compare the current month and year  
          //with the month and yearthe file was  
          //last modified  
  		java.nio.file.attribute.FileTime fileTimeCreation = view.lastModifiedTime();
  		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String dateCreated = df.format(fileTimeCreation.toMillis());
		Date createdDate = null;
		try {
			createdDate = df.parse(dateCreated);
			System.out.println(dateCreated);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
          if(dateFrom==null&&dateTo!=null) {return(createdDate.before(dateTo));}
         if(dateTo == null&&dateFrom!=null){return(createdDate.after(dateFrom));}
          if(dateTo!=null&&dateFrom!=null) return(createdDate.before(dateTo)&&createdDate.after(dateFrom));
          if(dateTo==null&&dateFrom==null) return true;
          
          return true;
    }   

    public String getDescription()  
    {  
        return "FileFilter";  
    }


}  