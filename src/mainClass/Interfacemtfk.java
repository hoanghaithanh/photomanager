package mainClass;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.PopupMenu;
import java.awt.Rectangle;
import java.awt.ScrollPane;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.SliderUI;

import org.apache.commons.io.FileUtils;
//xay dung giao dien chuong trinh
public class Interfacemtfk extends JFrame {
	
    //ma hoa chuc nang menu, button
	final static int PREVIOUS = 1;
	final static int NEXT = 2;
	final static int RIGHT = 3;
	final static int LEFT = 4;
	final static int OPEN = 5;
	final static int DELETE = 6;
	final static int PROPERTIES = 7;
	final static int COPY = 8;
	final static int FULLSCREEN = 9;
	final static int ZOOMBUTTONON = 10;
	final static int ZOOMBUTTONOFF = 11;
	final static int SAVETOFILE = 12;
	final static int FOCUSON = 13;
	final static int FOCUSOFF = 14;
	final static int CREATEALBUM = 15;
	final static int OPENALBUM = 16;
	final static int ADDFILETOALBUM = 17;
	final static int DELETEALBUM = 18;
	final static int BROWSEFOLDER = 19;
	final static int ALBUMFILTER = 20;

	// khai bao nut
	private JButton NextButton;
	private JButton PreviousButton;
	private JButton RightRotateButton;
	private JButton LeftRotateButton;
	private JButton FullScreenButton;
	private JButton DeleteButton;
	private JToggleButton FocusButton;
	private JToggleButton ZoomButton;
	

	
	//khai bao menu, menuitem
	private JMenuBar mainmenu;
	private JMenu Menu;
	private JMenu Album;
	private JMenu AlbumBrowser;
	private JMenuItem createAlbum;
	private JMenuItem openAlbum;
	private JMenuItem browseFolder;
	private JMenuItem albumfilter;
	private JMenuItem addPicturetoAlbum;
	private JMenuItem deleteAlbum;
	private JMenuItem Open;
	private JMenuItem Properties;
	private JMenuItem Copy;
	private JFrame slideframe;
	private JMenuItem save;

	// khai bao container
	private JPanel slidePanel;
	private JPanel Panel;
	private JPanel ButtonPanel;
	private JLabel Label;
	private JFileChooser chooser;
	private Box vbox;
	private int position=0;
	private String[] FileNames;
	private String Path;
	private String PathFolder;
	private ImageClass ImageFile;
	private Toolkit theKit = getToolkit();
    private Dimension size = theKit.getScreenSize();
	private FilenameFilter fileNameFilter;
	private JScrollPane pane;
	private albumClass albumControl = new albumClass();
	public Interfacemtfk() {
		
//setup cac thuoc tinh co ban cua jframe
		setTitle("Image Viewer");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());// The layout is BorderLayout
		
		//khoi tao Label hien thi hinh anh
		Label = new JLabel();
		Label.setVerticalTextPosition(SwingConstants.BOTTOM);
		Label.setHorizontalAlignment(SwingConstants.CENTER);
		Label.setHorizontalTextPosition(SwingConstants.CENTER);
		
		//Setup Previous Button
		PreviousButton = new JButton();
		PreviousButton.setMargin(new Insets(0,0,0,0));
		Image img = new ImageIcon(this.getClass().getResource("/prev.png")).getImage();
		PreviousButton.setIcon(new ImageIcon(img ));
		PreviousButton.setEnabled(false);
		PreviousButton.addActionListener(new ActionListener() { // Register an
																// actionListener
																// for the
																// PreviousButton
			public void actionPerformed(ActionEvent evt) {
				ButtonActionPerformed(PREVIOUS, evt);
			}
		});
		
		//Setup Focus Button
		FocusButton = new JToggleButton("");
		FocusButton.setMargin(new Insets(0,0,0,0));
		FocusButton.setEnabled(false);
		img = new ImageIcon(this.getClass().getResource("/focus.png")).getImage();
		FocusButton.setIcon(new ImageIcon(img));
		FocusButton.addItemListener(new ItemListener() {
			   public void itemStateChanged(ItemEvent ev) {
				      if(ev.getStateChange()==ItemEvent.SELECTED){
				        try {
							ButtonActionPerformed(FOCUSON, ev);
							Panel.remove(pane);
							Panel.revalidate();
							JPanel newpanel = new JPanel();
							newpanel.setPreferredSize(new Dimension(size.width/3, size.height*1/2));
							newpanel.setAlignmentX(JPanel.CENTER_ALIGNMENT);
							newpanel.setAlignmentY(JPanel.CENTER_ALIGNMENT);
							Panel.setAlignmentX(JPanel.CENTER_ALIGNMENT);
							Panel.setAlignmentY(JPanel.CENTER_ALIGNMENT);
							Label.setHorizontalAlignment(JLabel.CENTER);
							Label.setVerticalAlignment(JLabel.CENTER);
							Label.repaint();
							newpanel.setLayout(new BorderLayout());
							newpanel.add(Label, BorderLayout.CENTER);
							
							Panel.add(newpanel, BorderLayout.CENTER);
							Panel.revalidate();
							
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				      } else if(ev.getStateChange()==ItemEvent.DESELECTED){
				    	  try {
				    		Panel.removeAll();
							Panel.revalidate();
							ButtonActionPerformed(FOCUSOFF, ev);
							pane.getViewport().add(Label,BorderLayout.CENTER);
							Panel.add(pane, BorderLayout.CENTER);
							pane.repaint();
							Panel.repaint();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				      }
				   }
				});
		
		//Setup Zoom Button
		ZoomButton = new JToggleButton();
		ZoomButton.setMargin(new Insets(0,0,0,0));
		img = new ImageIcon(this.getClass().getResource("/zoom.png")).getImage();
		ZoomButton.setIcon(new ImageIcon(img));
		ZoomButton.setEnabled(false);
		ZoomButton.addItemListener(new ItemListener() {
			   public void itemStateChanged(ItemEvent ev) {
				      if(ev.getStateChange()==ItemEvent.SELECTED){
				        try {
							ButtonActionPerformed(ZOOMBUTTONON, ev);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				      } else if(ev.getStateChange()==ItemEvent.DESELECTED){
				    	  try {
							ButtonActionPerformed(ZOOMBUTTONOFF, ev);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				      }
				   }
				});
		
		//Setup Next Button
		NextButton = new JButton();
		img = new ImageIcon(this.getClass().getResource("/next.png")).getImage();
		NextButton.setIcon(new ImageIcon(img));
		NextButton.setMargin(new Insets(0,0,0,0));
		NextButton.setEnabled(false);
		NextButton.addActionListener(new ActionListener() { // Registers an
															// actionListener
															// for the
															// NextButton
			public void actionPerformed(ActionEvent evt) {
				ButtonActionPerformed(NEXT, evt);
			}
		});
		//setup Fullscreen Button
		FullScreenButton = new JButton();
		img = new ImageIcon(this.getClass().getResource("/fullscreen.png")).getImage();
		FullScreenButton.setIcon(new ImageIcon(img));
		FullScreenButton.setMargin(new Insets(0,0,0,0));
		FullScreenButton.setEnabled(false);
		FullScreenButton.addActionListener(new ActionListener() { 			
			public void actionPerformed(ActionEvent evt) {
				ButtonActionPerformed(FULLSCREEN, evt);
				}
			});
		//Setup nut quay phai
		RightRotateButton = new JButton();
		img = new ImageIcon(this.getClass().getResource("/right.png")).getImage();
		RightRotateButton.setIcon(new ImageIcon(img));
		RightRotateButton.setEnabled(false);
		RightRotateButton.setMargin(new Insets(0,0,0,0));
		RightRotateButton.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent evt) {
				ButtonActionPerformed(RIGHT, evt);
			}
		});
		
		//Setup nut quay trai
		LeftRotateButton = new JButton();
		img = new ImageIcon(this.getClass().getResource("/left.png")).getImage();
		LeftRotateButton.setIcon(new ImageIcon(img));
		LeftRotateButton.setMargin(new Insets(0,0,0,0));
		LeftRotateButton.setEnabled(false);
		LeftRotateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ButtonActionPerformed(LEFT, evt);
			}
		});
		
		//Setup Delete Button
		DeleteButton = new JButton("");
		DeleteButton.setEnabled(false);
		img = new ImageIcon(this.getClass().getResource("/delete.png")).getImage();
		DeleteButton.setIcon(new ImageIcon(img));
		DeleteButton.setMargin(new Insets(0,0,0,0));
		DeleteButton.addActionListener(new ActionListener() { // Registers
			
			public void actionPerformed(ActionEvent evt) {
				ButtonActionPerformed(DELETE, evt);
			}
		});
		
		//Setup main menu
		mainmenu = new JMenuBar();
		//menu Menu
		Menu = new JMenu("Menu");
		//menu Album
		Album = new JMenu("Album");
		//setup menuitem "Create Album"
		createAlbum = new JMenuItem("Create Album");
		createAlbum.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				ButtonActionPerformed(CREATEALBUM, evt);
			}
		});
		//setup menu "Album Browser"
		AlbumBrowser = new JMenu("Album Browser");
		//setup menuitem "Album Filter"
		albumfilter = new JMenuItem("Album Filter");
		albumfilter.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				ButtonActionPerformed(ALBUMFILTER, evt);
			}
		});
		//setup menuitem "Browse Album"
		browseFolder = new JMenuItem("Browse Album") ;
		browseFolder.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				ButtonActionPerformed(BROWSEFOLDER, evt);
			}
		});
		AlbumBrowser.add(browseFolder);
		AlbumBrowser.add(albumfilter);
		//setup menuitem "Open Album"
		openAlbum = new JMenuItem("Open Album");
		openAlbum.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				ButtonActionPerformed(OPENALBUM, evt);
			}
		});
		//setup menuitem "Add Picture  to Album"
		addPicturetoAlbum = new JMenuItem("Add Picture to Album");
		addPicturetoAlbum.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				ButtonActionPerformed(ADDFILETOALBUM, evt);
			}
		});
		//setup menuitem "Delete Album"
		deleteAlbum = new JMenuItem("Delete Album");
		deleteAlbum.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				ButtonActionPerformed(DELETEALBUM, evt);
			}
		});
		Album.add(createAlbum);
		Album.add(openAlbum);
		Album.add(AlbumBrowser);
		Album.add(addPicturetoAlbum);
		Album.add(deleteAlbum);
		Open = new JMenuItem("Open");
		Open.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ButtonActionPerformed(OPEN, evt);
			}
		});
		//setup menuitem "Save"
		save = new JMenuItem("Save");
		save.setEnabled(false);
		save.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				ButtonActionPerformed(SAVETOFILE, evt);
			}
		});
		//setup menuitem "properties"
		Properties = new JMenuItem("Properties");
		Properties.setEnabled(false);
		Properties.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ButtonActionPerformed(PROPERTIES, evt);
			}
		});
		//setup menuitem "copy"
		Copy = new JMenuItem("Copy");
		Copy.setEnabled(false);
		Copy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ButtonActionPerformed(COPY, evt);
			}
		});
		
		Menu.add(Open);
		Menu.add(save);
		Menu.add(Copy);
		Menu.add(Properties);
		mainmenu.add(Menu);
		mainmenu.add(Album);
		setJMenuBar(mainmenu);
		
		//tao panel chua scrollpane
		Panel = new JPanel();		
		Panel.setPreferredSize(new Dimension(size.width/3, size.height*1/2));
		Panel.setAlignmentX(CENTER_ALIGNMENT);
		Panel.setAlignmentY(CENTER_ALIGNMENT);
		Panel.setBackground(Color.WHITE);
		//tao scrollpane chua label
		pane = new JScrollPane();
		 pane.setPreferredSize(new Dimension(size.width/3, size.height*1/2));
		 
			pane.getViewport().add(Label);
			Panel.add(pane, BorderLayout.CENTER);

		//tao box co bo cuc theo chieu doc
		vbox = Box.createVerticalBox();
		vbox.setSize(size.width/4, size.height*2/3);
		vbox.setAlignmentX(CENTER_ALIGNMENT);
		vbox.setAlignmentY(CENTER_ALIGNMENT);
		vbox.add(Box.createVerticalStrut(20));
		vbox.add(Panel);
		vbox.add(Box.createVerticalStrut(20));
		
		//Tao panel chua cac nut trong tool
		ButtonPanel = new JPanel(); 
		ButtonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		ButtonPanel.add(ZoomButton);
		ButtonPanel.add(FocusButton);
		ButtonPanel.add(PreviousButton);
		ButtonPanel.add(FullScreenButton);
		ButtonPanel.add(NextButton);
		ButtonPanel.add(LeftRotateButton);
		ButtonPanel.add(RightRotateButton);
		ButtonPanel.add(DeleteButton);
		ButtonPanel.setOpaque(false);
		
		vbox.add(ButtonPanel);
		vbox.setBackground(Color.WHITE);
		add(vbox);
		
		setLocation(size.width / 5, size.height / 10);
		setMinimumSize(new java.awt.Dimension(900,800));
	}
//Ham xu ly chuc nang nut
	private void ButtonActionPerformed(int check, ActionEvent evt) {
		switch (check) {
		//code xu ly nut fullscreen
		case FULLSCREEN:

			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		    GraphicsDevice gs = ge.getDefaultScreenDevice();
			JFrame frame = new JFrame("Test");
			frame.setBackground(Color.BLACK);
			JPanel picpane = new JPanel();
			picpane.setBackground(Color.BLACK);
			JLabel piclabel = new JLabel();
			piclabel.setBackground(Color.BLACK);
			FlowLayout flow = new java.awt.FlowLayout(java.awt.FlowLayout.CENTER);
			picpane.setLayout(flow);
			BufferedImage image = new BufferedImage( (int) ((int)ImageFile.getImageBuffer().getWidth()*(1.0*size.getHeight()/ImageFile.getImageBuffer().getHeight())),(int) size.getHeight(),BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = image.createGraphics();
		     g.drawImage(ImageFile.getImageBuffer(), 0, 0, (int) ((int)ImageFile.getImageBuffer().getWidth()*(1.0*size.getHeight()/ImageFile.getImageBuffer().getHeight())),(int) size.getHeight(), this);
		    g.dispose();
		    ImageIcon icon = new ImageIcon(image);
		    piclabel.setIcon(icon);
		    picpane.add(piclabel);
			 KeyListener keyln = new KeyAdapter(){
				 public void keyPressed(KeyEvent ke) {  // handler
				        if(ke.getKeyCode() == ke.VK_ESCAPE) {
				        	frame.dispose();
				        }
				 }
				 };
			 MouseListener ml = new MouseAdapter ()
			    {
				 	
			        public void mouseClicked (MouseEvent event)
			        {
			        	position++;
			        	if(position>=FileNames.length)
			        	{
			        		frame.dispose();
			        		position--;
			        	}
			        	else{
						if(position == FileNames.length-1)
						   {
							   NextButton.setEnabled(false); 
						   }
						ImageFile = new ImageClass(PathFolder+"//"+FileNames[position],size.height*2/5);
						Label.setIcon(ImageFile.getIcon());
						BufferedImage image = new BufferedImage( (int) ((int)ImageFile.getImageBuffer().getWidth()*(1.0*size.getHeight()/ImageFile.getImageBuffer().getHeight())),(int) size.getHeight(),BufferedImage.TYPE_INT_ARGB);
						Graphics2D g = image.createGraphics();
					     g.drawImage(ImageFile.getImageBuffer(), 0, 0, (int) ((int)ImageFile.getImageBuffer().getWidth()*(1.0*size.getHeight()/ImageFile.getImageBuffer().getHeight())),(int) size.getHeight(), null);
					    g.dispose();
					    ImageIcon icon = new ImageIcon(image);
					    piclabel.setIcon(icon);
						frame.validate();
					    gs.setFullScreenWindow(frame);
			        }
			        }
			        
			    };
			    frame.addMouseListener(ml);
			    frame.addKeyListener(keyln);
		    frame.add(picpane);
		    gs.setFullScreenWindow(frame);
		    frame.validate();
			break;
			
			//code xu ly menuitem properties
		case PROPERTIES:
			ImageFile.GetInfo();
			break;
			//code xu ly nut 
		case COPY:
			JFileChooser chooser2 = new JFileChooser();
			chooser2.setCurrentDirectory(new File(System.getProperty("user.dir")+"\\album"));
			chooser2.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int returnval = chooser2.showOpenDialog(this);
			if(returnval == JFileChooser.APPROVE_OPTION)
			{
				String Path = chooser2.getSelectedFile().getPath();
				
				String ftype=ImageFile.getImageName().substring(ImageFile.getImageName().lastIndexOf('.')+1);
				File file = new File(Path+"//"+ImageFile.getImageName());
				 try{
					    
					    ImageIO.write(ImageFile.getImageOrigin(),ftype,file);
					     }catch(IOException e){System.out.println("Error in saving the file");
					     }
			}
			break;
			
			//code xu ly menuitem open
		case OPEN:
			position = 0;
			chooser = new JFileChooser();
			chooser.setCurrentDirectory(new File(System.getProperty("user.dir")+"\\album"));
			FileNameExtensionFilter filter = new FileNameExtensionFilter("Image files", "jpg", "gif","bmp","png");
			chooser.setFileFilter(filter);
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			int returnVal = chooser.showOpenDialog(this);
		      if(returnVal == JFileChooser.APPROVE_OPTION) {  
		   Path = chooser.getSelectedFile().getPath();
		   PathFolder = chooser.getSelectedFile().getParent();
		   File file = new File(PathFolder);
		   fileNameFilter = new FilenameFilter() {
			   
	            @Override
	            public boolean accept(File dir, String name) {
	               if(name.lastIndexOf('.')>0)
	               {
	                  // get last index for '.' char
	                  int lastIndex = name.lastIndexOf('.');
	                  
	                  // get extension
	                  String str = name.substring(lastIndex);
	                  
	                  // match path name extension
	                  if(str.equals(".png")|| str.equals(".jpg")||str.equals(".bmp")||str.equals(".gif")||str.equals(".PNG")|| str.equals(".JPG")||str.equals(".BMP")||str.equals(".GIF"))
	                  {
	                     return true;
	                  }
	               }
	               return false;
	            }
	         };
		   FileNames = file.list(fileNameFilter);
		   ImageFile = new ImageClass(Path, size.height*2/5);
		   while(!ImageFile.getImageName().equals(FileNames[position]))
		   {
		   position++;
		   }
		   PreviousButton.setEnabled(true);
		   NextButton.setEnabled(true);
		   if(position == 0)
		   {
			   PreviousButton.setEnabled(false);
		   }
		   if(position == FileNames.length-1)
		   {
			   NextButton.setEnabled(false);
		   }
		   Label.setEnabled(true);
		   Label.setIcon(ImageFile.getIcon());
		   setTitle(ImageFile.getImageName());
		   }
		      if(FileNames!=null){
		    FullScreenButton.setEnabled(true);
		    RightRotateButton.setEnabled(true);
		    LeftRotateButton.setEnabled(true);
		    ZoomButton.setEnabled(true);
		    save.setEnabled(true);
		    DeleteButton.setEnabled(true);
		    Properties.setEnabled(true);
		    Copy.setEnabled(true);
		    FocusButton.setEnabled(true);}
			break;
			//code xu ly nut previous
		case PREVIOUS:
			position--;
			if(position == 0)
			   {
				   PreviousButton.setEnabled(false);
			   }
			ImageFile = new ImageClass(PathFolder+"//"+FileNames[position],size.height*2/5);
			Label.setIcon(ImageFile.getIcon());
			NextButton.setEnabled(true);
			setTitle(ImageFile.getImageName());
			break;
			//code xu ly nut next
		case NEXT:
			position++;
			if(position == FileNames.length-1)
			   {
				   NextButton.setEnabled(false);
			   }
			ImageFile = new ImageClass(PathFolder+"//"+FileNames[position],size.height*2/5);
			Label.setIcon(ImageFile.getIcon());
			PreviousButton.setEnabled(true);
			setTitle(ImageFile.getImageName());
			break;
			//code xu ly nut right
		case RIGHT:
			ImageFile.RightRotateImage();
			Label.setIcon(ImageFile.getIcon());
			break;
			//code xu ly nut left rotate
		case LEFT:
			ImageFile.LeftRotateImage();
			Label.setIcon(ImageFile.getIcon());
			break;
			//code xu ly menuitem save
		case SAVETOFILE:
			ImageFile.SaveToFile();
			break;
			//code xu ly nut delete
		case DELETE:
			ImageFile.DeleteFile();
			File file = new File(PathFolder);
			FileNames = file.list(fileNameFilter);
			if(FileNames.length==0)
			{
				Label.setEnabled(false);
				FullScreenButton.setEnabled(false);
			    RightRotateButton.setEnabled(false);
			    LeftRotateButton.setEnabled(false);
				PreviousButton.setEnabled(false);
				NextButton.setEnabled(false);
				ZoomButton.setEnabled(false);
				save.setEnabled(false);
			    DeleteButton.setEnabled(false);
			    Properties.setEnabled(false);
			    Copy.setEnabled(false);
			    FocusButton.setEnabled(false);
				
			}
			else
			{
				
				position = 0;
				Path = PathFolder + "\\" + FileNames[0];
				ImageFile = new ImageClass(Path, size.height*2/5);
				Label.setIcon(ImageFile.getIcon());
				  
				setTitle(ImageFile.getImageName());
				PreviousButton.setEnabled(false);
				
				if(FileNames.length>=2)
				{
					NextButton.setEnabled(true);
				}
				else{
					NextButton.setEnabled(false);


			}
			}
			break;
			//code xu ly menuitem createalbum
		case CREATEALBUM:
			albumControl.createAlbum();
		break;
		//code xu ly menuitem add picture to album
		case ADDFILETOALBUM:
			albumControl.addFileToAlbum();
			break;
		//code xu ly menuitem open album
		case OPENALBUM:
			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(new File(System.getProperty("user.dir")+"\\album"));
			
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int returnValue = chooser.showOpenDialog(this);
		      if(returnValue == JFileChooser.APPROVE_OPTION) {  
		   PathFolder = chooser.getSelectedFile().getPath();
		   File file1 = new File(PathFolder);
		   if (!file1.getParent().equals(System.getProperty("user.dir")+"\\album"))
		   {
			   JOptionPane.showMessageDialog(null, "Album khong hop le");
			   break;
		   }
		   FileNames = file1.list(fileNameFilter);
			if(FileNames.length==0)
			{
				Label.setEnabled(false);
				FullScreenButton.setEnabled(false);
			    RightRotateButton.setEnabled(false);
			    LeftRotateButton.setEnabled(false);
				PreviousButton.setEnabled(false);
				NextButton.setEnabled(false);
				ZoomButton.setEnabled(false);
				save.setEnabled(false);
			    DeleteButton.setEnabled(false);
			    Properties.setEnabled(false);
			    Copy.setEnabled(false);
			    FocusButton.setEnabled(false);
				
			}
			else
			{
				Label.setEnabled(true);
				position = 0;
				Path = PathFolder + "\\" + FileNames[0];
				ImageFile = new ImageClass(Path, size.height*2/5);
				Label.setIcon(ImageFile.getIcon());
				 
				setTitle(ImageFile.getImageName());
				FullScreenButton.setEnabled(true);
			    RightRotateButton.setEnabled(true);
			    LeftRotateButton.setEnabled(true);
			    ZoomButton.setEnabled(true);
			    save.setEnabled(true);
			    DeleteButton.setEnabled(true);
			    Properties.setEnabled(true);
			    Copy.setEnabled(true);
			    FocusButton.setEnabled(true);}
				PreviousButton.setEnabled(false);
				
				if(FileNames.length>=2)
				{
					NextButton.setEnabled(true);
				}
				else{
					NextButton.setEnabled(false);


			}
		      }
			break;
			//code xu ly menu item browse album
		case BROWSEFOLDER:
			
			JFileChooser chooser6 = new JFileChooser();
			chooser6.setCurrentDirectory(new File(System.getProperty("user.dir")+"\\album"));
			chooser6.showDialog(this, "Done!");
			break;
			//code xu ly menuitem albumfilter
		case ALBUMFILTER:
			albumControl.albumFilter();
			break;
			//code xu ly menuitem deletealbum
		case DELETEALBUM:
			
		    			  if(albumControl.deleteAlbum())
		    			  {
		    				  JOptionPane.showMessageDialog(null, "Da xoa Album thanh cong");
		    				  save.setEnabled(false);
		    				  Properties.setEnabled(false);
		    				  Copy.setEnabled(false);
		    				  FullScreenButton.setEnabled(false);
		    				    RightRotateButton.setEnabled(false);
		    				    LeftRotateButton.setEnabled(false);
		    					PreviousButton.setEnabled(false);
		    					NextButton.setEnabled(false);
		    					ZoomButton.setEnabled(false);
		    					save.setEnabled(false);
		    				    DeleteButton.setEnabled(false);
		    				    Properties.setEnabled(false);
		    				    Copy.setEnabled(false);
		    				    FocusButton.setEnabled(false);
		    			  }
		    			  else
		    			  {
		    				  JOptionPane.showMessageDialog(null, "Xoa Album that bai");
		    			  }
			break;
		}
	}
		
		
	//ham xu ly cac toggle button
	private void ButtonActionPerformed(int check, ItemEvent ev) throws IOException {
		switch(check)
		{
		//code xu ly khi nguoi dung an zoombutton
		case ZOOMBUTTONON: 
			slidePanel = new JPanel();
			JSlider slider = new JSlider();
			slideframe = new JFrame();
			slideframe.setAlwaysOnTop(true);
			slideframe.setDefaultCloseOperation(EXIT_ON_CLOSE);
			slider.setMaximum(5);
			slider.setMinimum(1);
			slider.setMinorTickSpacing(1);
			slider.setSize(120,10);
			slider.setValue(1);
			slider.setVisible(true);
			slider.addChangeListener(new ChangeListener(){

				@Override
				public void stateChanged(ChangeEvent arg0) {
					
					
					int newImageWidth = (int) (ImageFile.getIcon().getIconWidth() * (1+((1.0*slider.getValue()-1)/4)));
					int newImageHeight = (int) (ImageFile.getIcon().getIconHeight() * (1+((1.0*slider.getValue()-1)/4)));
					BufferedImage resizedImage = new BufferedImage(newImageWidth , newImageHeight, ImageFile.getImageBuffer().getType());
					Graphics2D g = resizedImage.createGraphics();
					g.drawImage(ImageFile.getImageBuffer(), 0, 0, newImageWidth , newImageHeight , null);
					g.dispose();
					
					Label.setIcon(new ImageIcon(resizedImage));
					
					//Panel.add(new JScrollBar());
					
				}
				
			});
			slidePanel.setPreferredSize(slider.getSize());
			slidePanel.add(slider);
			slidePanel.setVisible(true);
			slideframe.setSize(200, 30);
			slideframe.add(slidePanel);
			slidePanel.setVisible(true);
			Point point = new Point(ZoomButton.getLocationOnScreen());
			Point newpoint = new Point(point.x, point.y-30);
			slideframe.setLocation(newpoint);
			slideframe.setUndecorated(true);
			slideframe.setVisible(true);
			
			break;
		//code xu ly khi nguoi dung bam esc hoac click chuot het folder
		case ZOOMBUTTONOFF:
			slideframe.dispose();
			break;
			//code xu ly khi nguoi dung bam nut focus
		case FOCUSON:
			int newImageWidth = (int) (ImageFile.getIcon().getIconWidth() * 2);
			int newImageHeight = (int) (ImageFile.getIcon().getIconHeight() * 2);
			BufferedImage resizedImage = new BufferedImage(newImageWidth , newImageHeight, ImageFile.getImageBuffer().getType());
			Graphics2D g = resizedImage.createGraphics();
			g.drawImage(ImageFile.getImageBuffer(), 0, 0, newImageWidth , newImageHeight , null);
			g.dispose();
			Label.setIcon(new ImageIcon(resizedImage));
			break;
			
			
		case FOCUSOFF:
			Label.setIcon(ImageFile.getIcon());
			break;
			
		}
		
	}
}
