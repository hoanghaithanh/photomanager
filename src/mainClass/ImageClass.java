package mainClass;
import java.awt.*;
import java.awt.Image;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.image.*;
import java.awt.color.*;
import javax.swing.filechooser.*;

import java.io.*;
import java.awt.*;
import java.awt.geom.*;
import javax.imageio.*;
import javax.imageio.stream.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.geom.AffineTransform;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import java.awt.*;

 class ImageClass{
	private String Path;
	private ImageIcon icon;
	private BufferedImage ImageBuffer;
	private BufferedImage ImageOrigin;

	private String ImageName;
	private int SizeHeight;
	private int SizeWidth;
	int ImageHeight;
	
	public BufferedImage getImageBuffer(){
		return ImageBuffer;
	}
	public BufferedImage getImageOrigin(){
		return ImageOrigin;
	}
	public ImageClass(String Path, int Height)
	{
		File file = new File(Path);
		
		try
		{
			ImageBuffer = ImageIO.read(file); //Read the image from the file.
			ImageOrigin = ImageBuffer;
	 }catch(IOException ie){
	javax.swing.JOptionPane.showMessageDialog(null,"Error reading image file","Error",
												javax.swing.JOptionPane.ERROR_MESSAGE);
	 }
		this.ImageHeight = Height;
		this.Path = Path;
		ImageName = file.getName();
		SizeHeight = Height;
		SizeWidth = (int)(ImageBuffer.getWidth()*((1.0*Height/ImageBuffer.getHeight())));
		icon = new ImageIcon(ImageBuffer);
		java.awt.Image image = icon.getImage().getScaledInstance(SizeWidth, SizeHeight, Image.SCALE_REPLICATE);
		icon = new ImageIcon(image);
	}
	public ImageIcon getIcon()
	{
		return icon;
	}
	
	public String getImageName() {
		return ImageName;
	}
	public void RightRotateImage()
	{
       BufferedImage rotatedImage = new BufferedImage(ImageBuffer.getHeight(),ImageBuffer.getWidth(),BufferedImage.TYPE_INT_RGB);
       Graphics2D g = rotatedImage.createGraphics();
       AffineTransform at = new AffineTransform();
       at.translate(rotatedImage.getWidth()/2, rotatedImage.getHeight()/2);
       at.rotate(Math.PI/2);
       at.translate(-ImageBuffer.getWidth()/2, -ImageBuffer.getHeight()/2);
       g.drawImage(ImageBuffer,at,null);
       g.dispose();
       ImageBuffer = rotatedImage;
       SizeWidth = (int)(ImageBuffer.getWidth()*((1.0*SizeHeight/ImageBuffer.getHeight())));
       icon = new ImageIcon(ImageBuffer);
	   java.awt.Image image = icon.getImage().getScaledInstance(SizeWidth, SizeHeight, Image.SCALE_REPLICATE);
	   icon = new ImageIcon(image);
       
       
	}
	
	public void LeftRotateImage()
	{
		BufferedImage rotatedImage = new BufferedImage(ImageBuffer.getHeight(),ImageBuffer.getWidth(),BufferedImage.TYPE_INT_RGB);
	       Graphics2D g = rotatedImage.createGraphics();
	       AffineTransform at = new AffineTransform();
	       at.translate(rotatedImage.getWidth()/2, rotatedImage.getHeight()/2);
	       at.rotate(-Math.PI/2);
	       at.translate(-ImageBuffer.getWidth()/2, -ImageBuffer.getHeight()/2);
	       g.drawImage(ImageBuffer,at,null);
	       g.dispose();
	       ImageBuffer = rotatedImage;
	       SizeWidth = (int)(ImageBuffer.getWidth()*((1.0*SizeHeight/ImageBuffer.getHeight())));
	       icon = new ImageIcon(ImageBuffer);
		   java.awt.Image image = icon.getImage().getScaledInstance(SizeWidth, SizeHeight, Image.SCALE_REPLICATE);
		   icon = new ImageIcon(image);
	}
	
	
	public void SaveToFile()
	{
		String ftype=ImageName.substring(ImageName.lastIndexOf('.')+1);
		File file = new File(this.Path);
		
		  try{
		   
		   
		    ImageIO.write(ImageBuffer,ftype,file);
		     }catch(IOException e){System.out.println("Error in saving the file");
		     }
	}
	
	public String getFileExtension(File file) {
	    String name = file.getName();
	    try {
	        return name.substring(name.lastIndexOf(".") + 1);
	    } catch (Exception e) {
	        return "";
	    }
	}
	public void DeleteFile()
	{
		try
		{
			File file = new File(this.Path);
			file.delete();
		}
		catch(Exception e)
		{
			System.out.println("Error in delete the file");
		}
	}
	public void GetInfo()
	{
		java.nio.file.Path path = Paths.get(this.Path);
		BasicFileAttributes view = null;
		try {
		    view = Files.getFileAttributeView( path, BasicFileAttributeView.class ).readAttributes();
		} catch ( IOException ex ) {
		    
		}

		java.nio.file.attribute.FileTime fileTimeCreation = view.creationTime();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String dateCreated = df.format(fileTimeCreation.toMillis());
		java.nio.file.attribute.FileTime fileTimeLastModified = view.lastModifiedTime();
		String dateModified = df.format(fileTimeLastModified.toMillis());
		String message = "Name: " +ImageName+ "\nLocation: " + path.getParent() +"\nDate Created: "+dateCreated+"\nDate Modified: "+dateModified+"\nSize: "+view.size()/1024+" Kb";
		JOptionPane.showMessageDialog(null, message, "Properties", JOptionPane.INFORMATION_MESSAGE);

		
		
	}
	
	
}
